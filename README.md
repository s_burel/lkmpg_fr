# README #

Ce dépôt contient le "Linux Kernel Module Programming Guide" dans sa version 4.15.2, traduit de l'anglais.

### Que faire ? ###

Pour lire ce guide dans votre format privilégié, utilisez la commande make !

Si toutefois le format ne vous convient pas, vous pouvez toujours le créer vous-même facilement grace à la commande pandoc.