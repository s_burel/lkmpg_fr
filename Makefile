name=LKMPG-`cat version.txt`
org=$(name).org

all: convert create_examples

create_examples:
	./create_example_from_guide.sh

convert:
	cp LKMPG.org $(name).org
	pandoc -f org -t odt -o $(name).odt $(org) &
	pandoc -f org -t markdown -o $(name).md $(org) &
	pandoc -f org -t html -o $(name).html $(org) &
	pandoc -f org -t plain -o $(name).txt $(org) &

clean :
	rm $(name).org
	rm $(name).odt
	rm $(name).md
	rm $(name).html
	rm $(name).txt
	rm -r examples